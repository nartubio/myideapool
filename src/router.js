import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Default from './layouts/default'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'root',
      component: Default,
      redirect: { name: 'my-ideas' },
      children: [
        {
          path: '/sign-up',
          name: 'sign-up',
          component: () => import(/* webpackChunkName: "about" */ './views/SignUp.vue')
        },
        {
          path: '/log-in',
          name: 'log-in',
          component: () => import(/* webpackChunkName: "about" */ './views/LogIn.vue')
        },
        {
          path: '/my-ideas',
          name: 'my-ideas',
          component: () => import(/* webpackChunkName: "about" */ './views/MyIdeas.vue'),
          meta: {
            requiresAuth: true
          }
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isLoggedIn) {
      next({
        path: '/log-in'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
