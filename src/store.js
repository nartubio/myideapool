import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],

  state: {
    user: null,
    jwt: null,
    refresh_token: null
  },

  getters: {
    isLoggedIn (state) {
      return state.user !== null
    }
  },

  mutations: {
    storeUser (state, user) {
      state.user = user
    },

    clearUser (state) {
      state.user = null
    },

    storeJWT (state, jwt) {
      state.jwt = jwt
    },

    clearJWT (state) {
      state.jwt = null
    },

    storeRefreshToken (state, refresh_token) {
      state.refresh_token = refresh_token
    },

    clearRefreshToken (state) {
      state.refresh_token = null
    }
  },

  actions: {

  }
})
