import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import PortalVue from 'portal-vue'
import axios from 'axios'
import VeeValidate from 'vee-validate'

Vue.use(PortalVue)
Vue.use(VeeValidate)

axios.defaults.baseURL = process.env.VUE_APP_API_ENDPOINT
axios.defaults.headers.common['x-access-token'] = store.state.jwt
axios.interceptors.response.use(function (response) {
  return response
}, async function (error) {
  if (error.response.status === 401 && store.state.refresh_token) {
    try {
      const response = await axios.post('/access-tokens/refresh', {
        refresh_token: store.state.refresh_token
      })

      store.commit('storeJWT', response.data.jwt)
      axios.defaults.headers.common['x-access-token'] = store.state.jwt
      error.config.headers['x-access-token'] = store.state.jwt

      return axios.request(error.config)
    } catch (e) {
      store.commit('clearJWT')
      store.commit('clearRefreshToken')
      store.commit('clearUser')

      return Promise.reject(error)
    }
  }
  return Promise.reject(error)
})

import './assets/styles.css'
import 'animate.css/animate.min.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
