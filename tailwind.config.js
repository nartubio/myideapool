module.exports = {
  theme: {
    extend: {
      colors: {
        green: 'rgba(0, 168, 67, 1)',
        black: 'rgba(42, 56, 66, 1)',
        'gray-250': 'rgb(248, 248, 248)',
        backdrop: 'rgba(0, 0, 0, .5)'
      }
    }
  },
  variants: {
    opacity: ['responsive', 'hover']
  },
  plugins: [
    // Some useful comment
  ]
}
